namespace Arachnid.Core.Benchmarks

open BenchmarkDotNet.Attributes
open Hopac
open Arachnid.Core
open Arachnid.Core.Operators

[<Config(typeof<CoreConfig>)>]
type RunArachnid () =
    let key_ =
        State.value_ "key"

    [<Benchmark>]
    member x.Arachnid () =
        let initialState =
            let newDictionary = System.Collections.Generic.Dictionary<_,_>() :> Environment
            State.create newDictionary
        let m = arachnid {
            let! v1 = Arachnid.Optic.get key_
            do! Arachnid.Optic.set key_ (Some (42UL :: (v1 |> Option.defaultValue [])))
            do! Arachnid.Optic.map key_ (Option.map (List.map ((*) 2UL)))
            let! v2 = Arachnid.Optic.get key_
            return v1, v2
        }
        m initialState
        |> Hopac.run

[<Config(typeof<CoreConfig>)>]
type HandleOwinMidFunc () =
    let key_ =
        State.value_ "key"

    let myArachnid = arachnid {
        let! r = Arachnid.Optic.get key_
        do! Arachnid.Optic.set key_ (Some 42UL)
        let! _ = Arachnid.Optic.get key_
        return Halt
    }

    let next : OwinAppFunc = OwinAppFunc (fun e -> System.Threading.Tasks.Task.CompletedTask)
    let omf : OwinMidFunc = OwinMidFunc.ofArachnid myArachnid

    [<Benchmark>]
    member __.OwinMidFunc () =
        let env = System.Collections.Generic.Dictionary<_,_>()
        omf.Invoke(next).Invoke(env).Wait()
