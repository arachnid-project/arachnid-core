﻿namespace Arachnid.Core

open System

// Operators

/// Symbolic operators for commonly used functions around core Arachnid
/// functionality, particularly monadic operations, including monadic optic
/// operations.

module Operators =
    // Common

    // Symbolic operators for common monadic functions, using common operators
    // to allow for familiarity and some compliance with effective "standards".

    /// The Apply (Arachnid.apply) function for Arachnid function types, taking a
    /// function Arachnid<'a -> 'b> and a Arachnid<'a> and returning a Arachnid<'b>.

    let inline (<*>) a2Fb aF =
        a2Fb |> Arachnid.apply aF

    /// The Bind (Arachnid.bind) function for Arachnid, taking a Arachnid<'a> and a
    /// function 'a -> Arachnid<'b> and returning a Arachnid<'b>.

    let inline (>>=) aF a2bF =
        aF |> Arachnid.bind a2bF

    /// The reversed Bind function for Arachnid.

    let inline (=<<) a2bF aF =
        aF |> Arachnid.bind a2bF

    /// The Kleisli composition function for Arachnid, taking a function
    /// 'a -> Arachnid<'b> and a function 'b -> Arachnid<'c> and returning a function
    /// 'a -> Arachnid<'c>.

    let inline (>=>) a2bF b2cF =
        a2bF >> Arachnid.bind b2cF

    /// The reversed Kleisli composition function for Arachnid.

    let inline (<=<) b2cF a2bF =
        a2bF >> Arachnid.bind b2cF

    /// The Left Combine (Arachnid.combine) function for Arachnid, taking two
    /// Arachnid<_> functions, composing their execution and returning the result
    /// of the first function.

    let inline ( *>) xF aF =
        xF |> Arachnid.combine aF

    /// The Right Combine (Arachnid.combine) function for Arachnid, taking two
    /// Arachnid<_> functions, composing their execution and returning the result
    /// of the second function.

    let inline ( <*) aF xF =
        xF |> Arachnid.combine aF

    /// The Map (Arachnid.map) function for Arachnid, taking a function 'a -> b' and
    /// a function Arachnid<'a> and returning a Arachnid<'b>.

    let inline (<!>) a2b aF =
        aF |> Arachnid.map a2b

    // Optic

    // Operators for applying optic based functions to the State instance,
    // wrapping the Arachnid.Optic.* functionality.

    /// The optical get function (Arachnid.Optic.get), used to get a value within
    /// the current computation State given an optic from State to the required
    /// value.

    let inline (!.) o =
        Arachnid.Optic.get o

    /// The optical set function (Arachnid.Optic.set), used to set a value within
    /// the current computation State given an optic from State to the required
    /// value and an instance of the required value.

    let inline (.=) o v =
        Arachnid.Optic.set o v

    /// The optical map function (Arachnid.Optic.map), used to map a value within
    /// the current computation State given an optic from the State the required
    /// value and a function from the current value to the new value (a
    /// homomorphism).

    let inline (%=) o f =
        Arachnid.Optic.map o f

    // Pipeline

    // Pipeline composition operators, allowing for an alternative chained
    // syntax to Pipeline.compose, a more natural expression of the effective
    // meaning.

    /// The Pipeline composition function (Pipeline.compose), used to compose
    /// two functions which may be - or may be inferred to be (see
    /// Pipeline.infer) - Pipeline functions, given the composition approach of
    /// executing functions sequentially until one returns Halt.

    /// In this case, the first function will always be executed, and if the
    /// result is Next, the second pipeline will be executed and the result of
    /// the second pipeline returned. Where the result of the first Pipeline is
    /// Halt, the second pipeline will never be executed, and the Halt result
    /// will be returned.

    let inline (>?=) p1 p2 =
        Pipeline.compose p1 p2

    // Obsolete

    // Backwards compatibility shims to make the 2.x-> 3.x transition
    // less painful, providing functionally equivalent options where possible.

    // To be removed for 4.x releases.

    [<AutoOpen>]
    module Obsolete =

        [<Obsolete ("Use !. instead.")>]
        let inline (!?.) o =
            Arachnid.Optic.get o

        [<Obsolete ("Use .= instead.")>]
        let inline (.?=) o v =
            Arachnid.Optic.set o v

        [<Obsolete ("Use %= instead.")>]
        let inline (%?=) o f =
            Arachnid.Optic.map o f
