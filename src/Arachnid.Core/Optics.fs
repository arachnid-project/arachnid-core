/// Optic based access to the Arachnid computation state, analogous to the
/// Optic.* functions exposed by Aether, but working within a Arachnid function
/// and therefore part of the Arachnid ecosystem.
namespace Arachnid.Core.Optics

open Arachnid.Core
#if HOPAC
open Hopac
#endif

[<RequireQualifiedAccess>]
module Arachnid =

    /// Optic based access to the Arachnid computation state, analogous to the
    /// Optic.* functions exposed by Aether, but working within a Arachnid function
    /// and therefore part of the Arachnid ecosystem.
    [<RequireQualifiedAccess>]
    module Optic =

        /// A function to get a value within the current computation State
        /// given an optic from State to the required value.
        let inline get o : Arachnid<'a> =
#if HOPAC
            Job.lift (fun s -> ArachnidResult.create (Aether.Optic.get o s) s)
#else
            fun s ->
                async.Return (ArachnidResult.create (Aether.Optic.get o s) s)
#endif

        /// A function to set a value within the current computation State
        /// given an optic from State to the required value and an instance of
        /// the required value.
        let inline set o v : Arachnid<unit> =
#if HOPAC
            Job.lift (fun (s: State) -> ArachnidResult.create () (Aether.Optic.set o v s))
#else
            fun (s: State) ->
                async.Return (ArachnidResult.create () (Aether.Optic.set o v s))
#endif

        /// A function to map a value within the current computation State
        /// given an optic from the State the required value and a function
        /// from the current value to the new value (a homomorphism).
        let inline map o f : Arachnid<unit> =
#if HOPAC
            Job.lift (fun (s: State) -> ArachnidResult.create () (Aether.Optic.map o f s))
#else
            fun (s: State) ->
                async.Return (ArachnidResult.create () (Aether.Optic.map o f s))
#endif

[<assembly:AutoOpen("Arachnid.Core.Optics")>]
do ()
