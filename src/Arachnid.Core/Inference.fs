﻿namespace Arachnid.Core

// Inference

[<AutoOpen>]
module Inference =

    /// Functions for inferring Arachnid types from suitable types which possess
    /// appropriate type signatures.

    [<RequireQualifiedAccess>]
    module Arachnid =

        // Inference

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member inline Arachnid (xF: Arachnid<'x>) =
                    xF

                static member inline Arachnid (_: unit) =
                    fun s ->
#if HOPAC
                        Hopac.Job.result ((), s)
#else
                        async.Return ((), s)
#endif

            let inline defaults (a: ^a, _: ^b) =
                    ((^a or ^b) : (static member Arachnid: ^a -> Arachnid<_>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        /// A function to return a Arachnid function given an instance of a type
        /// which has a suitable static Arachnid method. An existing Arachnid
        /// function will be returned as-is.

        let inline infer x =
            Inference.infer x
