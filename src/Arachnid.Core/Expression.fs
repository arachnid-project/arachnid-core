﻿namespace Arachnid.Core

// Expression

// A simple computation expression for working with Arachnid functions as an
// alternative to the function/operator based syntax also available. A basic
// builder is defined in and an instance of the builder.

// Types

/// The ArachnidBuilder type, implementing the basic underlying computation
/// expression builder required to supply Arachnid with computation expression
/// syntax for the core Arachnid<_> function type.

type ArachnidBuilder () =

    member __.Bind (aF: Arachnid<'a>, a2bF: 'a -> Arachnid<'b>) : Arachnid<'b> =
        aF |> Arachnid.bind a2bF

    member __.Delay (u2aF: unit -> Arachnid<'a>) : Arachnid<'a> =
        Arachnid.delay u2aF

    member __.Return (a: 'a) : Arachnid<'a> =
        Arachnid.init a

    member __.ReturnFrom (aF: Arachnid<'a>) : Arachnid<'a> =
        aF

    member __.Combine (xF: Arachnid<_>, aF: Arachnid<'a>) : Arachnid<'a> =
        xF |> Arachnid.combine aF

    member __.Zero () : Arachnid<unit> =
        Arachnid.empty

// Builder

// The instance of the ArachnidBuilder used to provide the arachnid computation
// expression syntax.

[<AutoOpen>]
module Builder =

    /// A computation expression for creating and working with Arachnid<_>
    /// function types using the various Arachnid functions defined for working
    /// with state, composition, etc.

    let arachnid =
        ArachnidBuilder ()
