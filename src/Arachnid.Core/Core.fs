﻿namespace Arachnid.Core

open Aether

#if HOPAC
open Hopac
#endif

// Core

// The common elements of all Arachnid based systems, namely the basic abstraction
// of an async state function over an OWIN environment, and tools for working
// with the environment in a functional and idiomatic way.

// Types

// Core types within the Arachnid codebase, representing the basic units of
// execution and composition, including the core async state carrying
// abstraction.

/// The core Arachnid type, representing a computation which is effectively a
/// State monad, with a concurrent return (the concurrency abstraction varies
/// based on the variant of Arachnid in use).

type Arachnid<'a> =
#if HOPAC
    State -> Job<ArachnidResult<'a>>
#else
    State -> Async<ArachnidResult<'a>>
#endif

and ArachnidResult<'a> =
#if STRUCT
    (struct ('a * State))
#else
    'a * State
#endif

/// The core Arachnid state type, containing the OWIN environment and other
/// metadata data structures which should be passed through a Arachnid
/// computation.

and State =
      /// The underlying request environment.
    { Environment: Environment
      /// Metadata associated with and bound to the lifetime of the request.
      Meta: Meta }

/// An alias for the commonly used OWIN data type of an
/// IDictionary<string,obj>.

and Environment =
    System.Collections.Generic.IDictionary<string, obj>

/// The Arachnid metadata data type containing data which should be passed through
/// a Arachnid computation but which is not relevant to non-Arachnid functions and so
/// is not considered part of the OWIN data model.

and Meta =
    /// Memoized value storage.
    { Memos: Map<System.Guid, obj> }


/// The Arachnid metadata data type containing data which should be passed through
/// a Arachnid computation but which is not relevant to non-Arachnid functions and so
/// is not considered part of the OWIN data model.

module Meta =
    /// A Lens to memoized values contained in request metadata.
    let memos_ : Lens<Meta,Map<System.Guid,obj>>=
        (fun x -> x.Memos),
        (fun m x -> { x with Memos = m })

    /// Provides an empty metadata object
    let empty =
        { Memos = Map.empty }

/// Patterns which allow destructuring Arachnid types.
[<AutoOpen>]
module Patterns =
    /// Destructures a `ArachnidResult` into a value and the associated state.
    let inline (|ArachnidResult|) (fr: ArachnidResult<'a>) =
        match fr with
#if STRUCT
        | struct (a, s) -> (a, s)
#else
        | a, s -> (a, s)
#endif


/// The result of a Arachnid operation, combining a value with the associated
/// request state.
[<RequireQualifiedAccess>]
module ArachnidResult =
    /// Destructures a `ArachnidResult`, selecting the associated state.
    let inline (|State|) (fr: ArachnidResult<'a>) =
        match fr with
#if STRUCT
        | struct (_, s) -> s
#else
        | _, s -> s
#endif

    /// Destructures a `ArachnidResult`, selecting the value.
    let inline (|Value|) (fr: ArachnidResult<'a>) =
        match fr with
#if STRUCT
        | struct (a, _) -> a
#else
        | a, _ -> a
#endif

    /// Constructs a `ArachnidResult` from a value and some associated state.
    let inline create a s : ArachnidResult<'a> =
#if STRUCT
        struct (a, s)
#else
        (a, s)
#endif

    /// Constructs a `ArachnidResult` from a value and some associated state.
    ///
    /// Equivalent to `create` with the arguments flipped.
    let inline createWithState s a : ArachnidResult<'a> =
        create a s

    /// A Lens from a `ArachnidResult` to its value.
    let value_ : Aether.Lens<ArachnidResult<'a>,'a> =
        (fun (Value a) -> a),
        (fun a (State s) -> create a s)

    /// A Lens from a `ArachnidResult` to its associated state.
    let state_ : Aether.Lens<ArachnidResult<'a>,State> =
        (fun (State s) -> s),
        (fun s (Value a) -> create a s)

// State

/// Basic optics for accessing elements of the State instance within the
/// current Arachnid function. The value_ lens is provided for keyed access
/// to the OWIN dictionary, and the memo_ lens for keyed access to the
/// memo storage in the Meta instance.

[<RequireQualifiedAccess>]
module State =
    open Aether.Operators

    /// A Lens from a `State` to its `Environment`.
    let environment_ : Lens<State,Environment> =
        (fun x -> x.Environment),
        (fun e x -> { x with Environment = e })

    /// A Lens from a `State` to associated metadata.
    let meta_ : Lens<State,Meta> =
        (fun x -> x.Meta),
        (fun m x -> { x with Meta = m })

    /// Creates a new `State` from a given `Environment`
    /// with no metadata.
    let create : Environment -> State =
        do ()
        fun (env : Environment) ->
            { Environment = env
              Meta = Meta.empty }

    /// A prism from the Arachnid State to a value of type 'a at a given string
    /// key.

    let key_<'a> k =
            environment_
        >-> Dict.key_<string,obj> k
        >?> box_<'a>

    /// A lens from the Arachnid State to a value of type 'a option at a given
    /// string key.

    /// When working with this lens as an optic, the Some and None cases of
    /// optic carry semantic meaning, where Some indicates that the value is or
    /// should be present within the State, and None indicates that the value
    /// is not, or should not be present within the State.

    let value_<'a> k =
            environment_
        >-> Dict.value_<string,obj> k
        >-> Option.mapIsomorphism box_<'a>

    /// A lens from the Arachnid State to a memoized value of type 'a at a given
    /// Guid key.

    /// When working with this lens as an optic, the Some and None cases of
    /// optic carry semantic meaning, where Some indicates that the value is or
    /// should be present within the State, and None indicates that the value
    /// is not, or should not be present within the State.

    let memo_<'a> i =
            meta_
        >-> Meta.memos_
        >-> Map.value_ i
        >-> Option.mapIsomorphism box_<'a>

// Arachnid

/// Functions and type tools for working with Arachnid abstractions, particularly
/// data contained within the Arachnid state abstraction. Commonly defined
/// functions for treating the Arachnid functions as a monad, etc. are also
/// included, along with basic support for static inference.

[<RequireQualifiedAccess>]
module Arachnid =

    // Common

    // Commonly defined functions against the Arachnid types, particularly the
    // usual monadic functions (bind, apply, etc.). These are commonly used
    // directly within Arachnid programming but are also used within the Arachnid
    // computation expression defined later.

    /// The init (or pure) function, used to raise a value of type 'a to a
    /// value of type Arachnid<'a>.

    let init (a: 'a) : Arachnid<'a> =
        ArachnidResult.create a
#if HOPAC
        >> Job.result
#else
        >> async.Return
#endif

    /// The map function, used to map a value of type Arachnid<'a> to Arachnid<'b>,
    /// given a function 'a -> 'b.

    let map (a2b: 'a -> 'b) (aF: Arachnid<'a>) : Arachnid<'b> =
        fun s ->
#if HOPAC
            aF s |> Job.map (fun (ArachnidResult (a, s1)) -> ArachnidResult.create (a2b a) s1)
#else
            async.Bind (aF s, fun (ArachnidResult (a, s1)) ->
                async.Return (ArachnidResult.create (a2b a) s1))
#endif

    /// Takes two Arachnid values and maps them into a function
    let map2 (a2b2c: 'a -> 'b -> 'c) (aF: Arachnid<'a>) (bF: Arachnid<'b>) : Arachnid<'c> =
        fun s ->
#if HOPAC
            aF s |> Job.bind (fun (ArachnidResult (a, s1)) ->
                bF s1 |> Job.map (fun (ArachnidResult (b, s2)) ->
                    ArachnidResult.create (a2b2c a b) s2))
#else
            async.Bind (aF s, fun (ArachnidResult (a, s1)) ->
                async.Bind (bF s1, fun (ArachnidResult (b, s2)) ->
                    async.Return (ArachnidResult.create (a2b2c a b) s2)))
#endif

    /// Takes two Arachnid values and maps them into a function
    let map3 (a2b2c2d: 'a -> 'b -> 'c -> 'd) (aF: Arachnid<'a>) (bF: Arachnid<'b>) (cF: Arachnid<'c>) : Arachnid<'d> =
        fun s ->
#if HOPAC
            aF s |> Job.bind (fun (ArachnidResult (a, s1)) ->
                bF s1 |> Job.bind (fun (ArachnidResult (b, s2)) ->
                    cF s2 |> Job.map (fun (ArachnidResult (c, s3)) ->
                        ArachnidResult.create (a2b2c2d a b c) s3)))
#else
            async.Bind (aF s, fun (ArachnidResult (a, s1)) ->
                async.Bind (bF s1, fun (ArachnidResult (b, s2)) ->
                    async.Bind (cF s2, fun (ArachnidResult (c, s3)) ->
                        async.Return (ArachnidResult.create (a2b2c2d a b c) s3))))
#endif

    /// The Bind function for Arachnid, taking a Arachnid<'a> and a function
    /// 'a -> Arachnid<'b> and returning a Arachnid<'b>.

    let bind (a2bF: 'a -> Arachnid<'b>) (aF: Arachnid<'a>) : Arachnid<'b> =
        fun s ->
#if HOPAC
            aF s |> Job.bind (fun (ArachnidResult (a, s1)) -> a2bF a s1)
#else
            async.Bind (aF s, fun (ArachnidResult (a, s1)) -> a2bF a s1)
#endif

    /// The apply function for Arachnid function types, taking a function
    /// Arachnid<'a -> 'b> and a Arachnid<'a> and returning a Arachnid<'b>.

    let apply (aF: Arachnid<'a>) (a2Fb: Arachnid<'a -> 'b>) : Arachnid<'b> =
        fun s ->
#if HOPAC
            a2Fb s |> Job.bind (fun (ArachnidResult (a2b, s1)) ->
                aF s1 |> Job.map (fun (ArachnidResult (a, s2)) ->
                    ArachnidResult.create (a2b a) s2))
#else
            async.Bind (a2Fb s, fun (ArachnidResult (a2b, s1)) ->
                async.Bind (aF s1, fun (ArachnidResult (a, s2)) ->
                    async.Return (ArachnidResult.create (a2b a) s2)))
#endif

    /// The Left Combine function for Arachnid, taking two Arachnid<_> functions,
    /// composing their execution and returning the result of the first
    /// function.

    let combine (aF: Arachnid<'a>) (xF: Arachnid<'x>) : Arachnid<'a> =
        fun s ->
#if HOPAC
            xF s |> Job.bind (fun (ArachnidResult.State s1) -> aF s1)
#else
            async.Bind (xF s, fun (ArachnidResult.State s1) -> aF s1)
#endif

    /// The Arachnid delay function, used to delay execution of a arachnid function
    /// by consuming a unit function to return the underlying Arachnid function.

    let delay (u2aF: unit -> Arachnid<'a>) : Arachnid<'a> =
#if HOPAC
        Job.delayWith (fun s -> u2aF () s)
#else
        fun s ->
            u2aF () s
#endif

    /// The identity function for Arachnid type functions.

    let identity (xF: Arachnid<_>) : Arachnid<_> =
        xF

    // Empty

    /// A simple convenience instance of an empty Arachnid function, returning
    /// the unit type. This can be required for various forms of branching logic
    /// etc. and is a convenience to save writing Arachnid.init () repeatedly.
    let empty : Arachnid<unit> =
        init ()

    /// The zero function, used to initialize a new function of Arachnid<unit>,
    /// effectively lifting the unit value to a Arachnid<unit> function.

    let zero () : Arachnid<unit> = empty

    // Extended

    // Some extended functions providing additional convenience outside of the
    // usual set of functions defined against Arachnid. In this case, interop with
    // the basic F# async system, and extended dual map function are given.

#if HOPAC

    /// Converts a Hopac Job to a Arachnid
    let fromJob (aJ: Job<'a>) : Arachnid<'a> =
        fun s ->
            aJ |> Job.map (ArachnidResult.createWithState s)

    /// Lifts a function generating a Hopac Job to one creating a Arachnid
    let liftJob (a2bJ: 'a -> Job<'b>) (a: 'a) : Arachnid<'b> =
        fun s ->
            a2bJ a |> Job.map (ArachnidResult.createWithState s)

    /// Binds a Hopac Job to a function generating a Arachnid
    let bindJob (a2bF: 'a -> Arachnid<'b>) (aJ: Job<'a>) : Arachnid<'b> =
        fun s ->
            aJ |> Job.bind (fun a -> a2bF a s)

#endif

    /// Converts an Async to a Arachnid
    let fromAsync (aA: Async<'a>) : Arachnid<'a> =
        fun s ->
#if HOPAC
            Job.fromAsync aA |> Job.map (ArachnidResult.createWithState s)
#else
            async.Bind (aA, fun a ->
                async.Return (ArachnidResult.create a s))
#endif

    /// Lifts a function generating an Async to one creating a Arachnid
    let liftAsync (a2bA: 'a -> Async<'b>) (a: 'a) : Arachnid<'b> =
        fun s ->
#if HOPAC
            a2bA a |> Job.fromAsync |> Job.map (ArachnidResult.createWithState s)
#else
            async.Bind (a2bA a, fun b ->
                async.Return (ArachnidResult.create b s))
#endif

    /// Binds an Async to a function generating a Arachnid
    let bindAsync (a2bF: 'a -> Arachnid<'b>) (aA: Async<'a>) : Arachnid<'b> =
        fun s ->
#if HOPAC
            Job.fromAsync aA |> Job.bind (fun a -> a2bF a s)
#else
            async.Bind (aA, fun a -> a2bF a s)
#endif

    // Memoisation

    /// A function supporting memoisation of parameterless Arachnid functions
    /// (effectively a fully applied Arachnid expression) which will cache the
    /// result of the function in the Environment instance. This ensures that
    /// the function will be evaluated once per request/response on any given
    /// thread.
    let memo<'a> (aF: Arachnid<'a>) : Arachnid<'a> =
        let memo_ = State.memo_<'a> (System.Guid.NewGuid ())

        fun s ->
            match Aether.Optic.get memo_ s with
            | Some memo ->
#if HOPAC
                Job.result (ArachnidResult.create memo s)
#else
                async.Return (ArachnidResult.create memo s)
#endif
            | _ ->
#if HOPAC
                aF s |> Job.map (fun (ArachnidResult (memo, s)) ->
                    (ArachnidResult.create memo (Aether.Optic.set memo_ (Some memo) s)))
#else
                async.Bind (aF s, fun (ArachnidResult (memo, s)) ->
                    async.Return (ArachnidResult.create memo (Aether.Optic.set memo_ (Some memo) s)))
#endif

    /// Optic based access to the Arachnid computation state, analogous to the
    /// Optic.* functions exposed by Aether, but working within a Arachnid function
    /// and therefore part of the Arachnid ecosystem.
    [<RequireQualifiedAccess>]
    module Optic =

        /// A function to get a value within the current computation State
        /// given an optic from State to the required value.
        let inline get o : Arachnid<'a> =
#if HOPAC
            Job.lift (fun s -> ArachnidResult.create (Aether.Optic.get o s) s)
#else
            fun s ->
                async.Return (ArachnidResult.create (Aether.Optic.get o s) s)
#endif

        /// A function to set a value within the current computation State
        /// given an optic from State to the required value and an instance of
        /// the required value.
        let inline set o v : Arachnid<unit> =
#if HOPAC
            Job.lift (fun (s: State) -> ArachnidResult.create () (Aether.Optic.set o v s))
#else
            fun (s: State) ->
                async.Return (ArachnidResult.create () (Aether.Optic.set o v s))
#endif

        /// A function to map a value within the current computation State
        /// given an optic from the State the required value and a function
        /// from the current value to the new value (a homomorphism).
        let inline map o f : Arachnid<unit> =
#if HOPAC
            Job.lift (fun (s: State) -> ArachnidResult.create () (Aether.Optic.map o f s))
#else
            fun (s: State) ->
                async.Return (ArachnidResult.create () (Aether.Optic.map o f s))
#endif
