# Arachnid Core

## Overview

Arachnid Core provides the basic universal abstractions and computation approach used throughout the Arachnid ecosystem. Web oriented computation expressions and monadic syntax options are provided, along with basic pipeline and compositional abstractions.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-core/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-core/commits/master)

## See Also

For more information see the [meta-repository for the Arachnid Web Stack](https://gitlab.com/arachnid-project/arachnid), along with the main [arachnid.io](https://arachnid.io) site.
